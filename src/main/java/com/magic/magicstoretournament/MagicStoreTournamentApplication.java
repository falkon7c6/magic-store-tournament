package com.magic.magicstoretournament;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@EnableEurekaClient
@SpringBootApplication
public class MagicStoreTournamentApplication {

    public static void main(String[] args) {
        SpringApplication.run(MagicStoreTournamentApplication.class, args);
    }

}
